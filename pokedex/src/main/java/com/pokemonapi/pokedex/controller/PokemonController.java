package com.pokemonapi.pokedex.controller;

import com.pokemonapi.pokedex.dtos.PokemonVO;
import com.pokemonapi.pokedex.service.AbilitiesService;
import com.pokemonapi.pokedex.service.MovesService;
import com.pokemonapi.pokedex.service.PokemonServices;
import com.pokemonapi.pokedex.service.TypesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600L, allowedHeaders = "*")
@Slf4j
@RequestMapping(value = "/pokemon", produces = MediaType.APPLICATION_JSON_VALUE)
public class PokemonController {

    @Autowired
    final PokemonServices pokemonServices;
    @Autowired
    final AbilitiesService abilitiesService;
    @Autowired
    final MovesService movesService;
    @Autowired
    final TypesService typesService;

    public PokemonController(PokemonServices pokemonServices, AbilitiesService abilitiesService,
                             MovesService movesService, TypesService typesService) {
        this.pokemonServices = pokemonServices;
        this.abilitiesService = abilitiesService;
        this.movesService = movesService;
        this.typesService = typesService;
    }

    //Método para buscar Pokemon no PokeAPI
    @GetMapping("/pokemon-in-web/{id}")
    public Mono<PokemonVO> getPokemonInInternet(@PathVariable String id) {
        Mono<PokemonVO> pokemonOptional = pokemonServices.findAndPokemonByIdNet(id);
        return pokemonOptional;
    }

    //Método para salvar Pokemons
    @PostMapping("/save-in-database/{id}")
    public ResponseEntity<Object> getPokemonById(@PathVariable String id) {
        if (pokemonServices.findByIdPokemon(id) != null) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("O Pokemon já foi salvo no DB");
        }
        Mono<PokemonVO> pokemon = pokemonServices.findAndPokemonByIdNet(id);
        pokemon.subscribe(pokemonVo -> {
            pokemonVo.converter(this.pokemonServices, this.abilitiesService, this.movesService, this.typesService);
        });
        return ResponseEntity.status(HttpStatus.OK).body("Pokemon salvo com sucesso!");
    }

    //Método para deletar Pokemon
    @DeleteMapping("/pokemon-delete-db/{id}")
    public ResponseEntity<Object> DeletePokemonInInternet(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(pokemonServices.delete(id));
    }

    //Método para ver todos as habilidades de cada Pokemon
    @GetMapping("/get-abilities-pokemon/{pokemonId}")
    public ResponseEntity<Object> getAllAbilitiesPokemon(@PathVariable Long pokemonId) {
        ResponseEntity<Object> abilities = pokemonServices.getAllAbilitiesForId(pokemonId);
        return ResponseEntity.status(HttpStatus.OK).body(abilities);
    }

    //Método para ver todos os movimentos de cada Pokemon
    @GetMapping("/get-moves-pokemon/{pokemonId}")
    public ResponseEntity<Object> getAllMovesPokemon(@PathVariable Long pokemonId) {
        ResponseEntity<Object> movimentos = pokemonServices.getAllMovesForId(pokemonId);
        return ResponseEntity.status(HttpStatus.OK).body(movimentos);
    }

    //Método para ver todos os tipos de cada Pokemon
    @GetMapping("/get-types-pokemon/{pokemonId}")
    public ResponseEntity<Object> getAllTypesPokemon(@PathVariable Long pokemonId) {
        ResponseEntity<Object> types = pokemonServices.getAllTypesForId(pokemonId);
        return ResponseEntity.status(HttpStatus.OK).body(types);
    }

    //Método para buscar Pokemon pelo id do banco
    @GetMapping("/get-one-database/{id}")
    public ResponseEntity<Object> getOnePokemonInDb(@PathVariable Long id) {
        ResponseEntity<Object> pokemon = pokemonServices.getOnePokemonInDb(id);
        return ResponseEntity.status(HttpStatus.OK).body(pokemon);
    }

    //GetMoves????
    //Método para buscar Pokemon no Banco pelo nome
    @GetMapping("/pokemon-for-name/{name}")
    public ResponseEntity getMovesPokemon(@PathVariable String name) {
        return ResponseEntity.ok(pokemonServices.searchPokemon(name));
    }

    //Método para ver todos habilidades do banco
    @GetMapping("/get-all-abilities")
    public ResponseEntity<Stream<String>> getAllAbilities() {
        return ResponseEntity.ok(abilitiesService.findAllAbilities());
    }

    //Método para ver todos movimentos do banco
    @GetMapping("/get-all-moves")
    public ResponseEntity<Stream<String>> getAllMoves() {
        return ResponseEntity.ok(movesService.findAllMoves());
    }

    //Método para ver todos os tipos do banco
    @GetMapping("/get-all-types")
    public ResponseEntity<Stream<String>> getAllTypes() {
        return ResponseEntity.ok(typesService.findAllTypes());
    }

    //Método para ver pokemons e seus atributos
    @GetMapping("/get-all-pokemons")
    public ResponseEntity<Object> getAllPokemon() {
        return pokemonServices.findAll();
    }

    @GetMapping("/mount-pokemons/{id}")
    public ResponseEntity<Object> mountPokemon(@PathVariable Long id) {
        List pokemonMount = new ArrayList();
        ResponseEntity<Object> pokemon = getOnePokemonInDb(id);
        ResponseEntity<Object> abilities = pokemonServices.getAllAbilitiesForId(id);
        ResponseEntity<Object> moves = pokemonServices.getAllMovesForId(id);
        ResponseEntity<Object> types = pokemonServices.getAllTypesForId(id);
        pokemonMount.add(pokemon);
        pokemonMount.add(abilities);
        pokemonMount.add(moves);
        pokemonMount.add(types);
        return ResponseEntity.status(HttpStatus.OK).body(pokemonMount);
    }
}
