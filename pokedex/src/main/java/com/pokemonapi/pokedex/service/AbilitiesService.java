package com.pokemonapi.pokedex.service;

import com.pokemonapi.pokedex.models.Abilities;
import com.pokemonapi.pokedex.repository.AbilitiesRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

@Service
@Slf4j
public class AbilitiesService {
    final AbilitiesRepository abilitiesRepository;

    public AbilitiesService(AbilitiesRepository abilitiesRepository){this.abilitiesRepository = abilitiesRepository;}

    public Abilities save (Abilities abilities) {return abilitiesRepository.save(abilities);}

    public Stream<String> findAllAbilities() {return abilitiesRepository.findAll().stream().map(Abilities::getName);}
}
