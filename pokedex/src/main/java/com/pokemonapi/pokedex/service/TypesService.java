package com.pokemonapi.pokedex.service;

import com.pokemonapi.pokedex.models.Types;
import com.pokemonapi.pokedex.repository.TypesRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

@Service
@Slf4j
public class TypesService {
    final TypesRepository typesRepository;

    public TypesService(TypesRepository typesRepository){this.typesRepository = typesRepository;}

    public Types save (Types types) {return typesRepository.save(types);}

    public Types findTypesById(Long id){ return typesRepository.getById(id);}

    public Stream<String> findAllTypes() {return typesRepository.findAll().stream().map(Types::getName);}
}
