package com.pokemonapi.pokedex.service;

import com.pokemonapi.pokedex.dtos.PokemonVO;
import com.pokemonapi.pokedex.models.Abilities;
import com.pokemonapi.pokedex.models.Moves;
import com.pokemonapi.pokedex.models.Pokemon;
import com.pokemonapi.pokedex.models.Types;
import com.pokemonapi.pokedex.repository.AbilitiesRepository;
import com.pokemonapi.pokedex.repository.MovesRepository;
import com.pokemonapi.pokedex.repository.PokemonRepository;
import com.pokemonapi.pokedex.repository.TypesRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Service
@Slf4j
public class PokemonServices {

    //Serviços e repositório das Habilidades
    final AbilitiesService abilitiesService;
    final AbilitiesRepository abilitiesRepository;

    //Serviços e repositório dos Movimentos
    final MovesService movesService;
    final MovesRepository movesRepository;

    //Serviços e Repositório dos Tipos;
    final TypesService typesService;
    final TypesRepository typesRepository;

    //Repositório Pokemon
    final PokemonRepository pokemonRepository;

    //Variável contendo a url da PokeApi
    WebClient webClient;

    //Construtor da url da PokeApi
    public PokemonServices(AbilitiesService abilitiesService, AbilitiesRepository abilitiesRepository, MovesService movesService,
                           MovesRepository movesRepository, TypesService typesService, TypesRepository typesRepository,
                           PokemonRepository pokemonRepository, WebClient.Builder builder) {

        this.abilitiesService = abilitiesService;
        this.abilitiesRepository = abilitiesRepository;

        this.movesService = movesService;
        this.movesRepository = movesRepository;

        this.typesService = typesService;
        this.typesRepository = typesRepository;

        this.pokemonRepository = pokemonRepository;

        webClient = builder.baseUrl("https://pokeapi.co/api/v2/").build();

    }

    //Método para salvar o Pokemon
    public Pokemon save (Pokemon pokemon) { return pokemonRepository.save(pokemon);}

    //Busca Pokemon pelo Id
    public Pokemon findByIdPokemon (String id) { return pokemonRepository.findByIdPokemon(id);}

    //Método para buscar todos os Pokemons
    public Stream<String> findAllPokemon(){ return pokemonRepository.findAll().stream().map(Pokemon::getName);};

    //Método para buscar Pokemon pelo nome
    public Stream<String> searchPokemon (String name) { return pokemonRepository.findByNameContaining(name).stream().map(Pokemon::getName);};

    //Deletar Pokemon
    public ResponseEntity<String> delete(Long id) {
        Optional<Pokemon> pokemonOptional = pokemonRepository.findById(id);
        if (!pokemonOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Pokemon não foi encontrado para deletar");
        }
        pokemonRepository.deleteById(pokemonOptional.get().getId());
        return ResponseEntity.status(HttpStatus.OK).body("Pokemon foi deletado com sucesso");
    }

    //Método para criar Pokemon
    public Pokemon createPokemon(Optional<Pokemon> pokemon) {
        Pokemon pokemonObj = new Pokemon();
        pokemonObj.setName(pokemon.get().getName());
        pokemonObj.setIdPokemon(pokemon.get().getIdPokemon());
        pokemonObj.setWeight(pokemon.get().getWeight());
        pokemonObj.setBaseExperience(pokemon.get().getBaseExperience());
        pokemonObj.setId(pokemon.get().getId());

        pokemonObj.setAbilities(pokemon.get().getAbilities());
        pokemonObj.setMoves(pokemon.get().getMoves());
        pokemonObj.setTypes(pokemon.get().getTypes());
        return pokemonObj;
    }

    //Método para buscar tudo
    public ResponseEntity<Object> findAll() {
        List<Pokemon> pokemons = pokemonRepository.findAll();
        if(pokemons.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Nenhum Pokemon encontrado");
        }
        return ResponseEntity.status(HttpStatus.OK).body(pokemons);
    }

    //Método para pegar todas as habilidades por Id
    public ResponseEntity<Object> getAllAbilitiesForId(Long pokemonId) {
        Optional validation = pokemonRepository.findById(pokemonId);
        if(!validation.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Nenhuma habilidade encontrado para este pokemon");
        }
        Pokemon pokemon = pokemonRepository.findById(pokemonId).get();
        Stream<String> result = pokemon.getAbilities().stream().map(Abilities::getName);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    //Método para pegar todos os movimentos por Id
    public ResponseEntity<Object> getAllMovesForId(Long pokemonId){
        Optional validation = pokemonRepository.findById(pokemonId);
        if(!validation.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Nenhum movimento encontrado para este Pokemon");
        }
        Pokemon pokemon = pokemonRepository.findById(pokemonId).get();
        Stream<String> result = pokemon.getMoves().stream().map(Moves::getName);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    //Método para pegar todos os tipos por Id
    public ResponseEntity<Object> getAllTypesForId(Long pokemonId) {
        Optional validation = pokemonRepository.findById(pokemonId);
        if(!validation.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Nenhum tipo encontrado para este pokemon");
        }
        Pokemon pokemon = pokemonRepository.findById(pokemonId).get();
        Stream<String> result = pokemon.getTypes().stream().map(Types::getName);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    //Busca o Pokemon pela PokeApi
    public Mono<PokemonVO> findAndPokemonByIdNet(String id){
        log.info("Buscando Pokemon pelo id [{}]", id);
        return webClient
                .get()
                .uri("/pokemon/" + id)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        error -> Mono.error(new RuntimeException("Verifique as informações passadas")))
                .bodyToMono(PokemonVO.class);
    }

    //Buscar somente um Pokemon no DB
    public ResponseEntity<Object> getOnePokemonInDb(Long id) {
        Optional <Pokemon> pokemonOptional = pokemonRepository.findById(id);
        if(!pokemonOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Nenhum Pokemon foi encontrado :(");
        }
        Pokemon pokemon = createPokemon(pokemonOptional);
        return ResponseEntity.status(HttpStatus.OK).body(pokemon);
    }

}
