package com.pokemonapi.pokedex.service;

import com.pokemonapi.pokedex.models.Moves;
import com.pokemonapi.pokedex.repository.MovesRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

@Service
@Slf4j
public class MovesService {
    final MovesRepository movesRepository;

    public MovesService(MovesRepository movesRepository){this.movesRepository = movesRepository;}

    public Moves save (Moves moves) {return movesRepository.save(moves);}

    public Moves findMovesById (Long id){ return movesRepository.getById(id);}

    public Stream<String> findAllMoves() {return movesRepository.findAll().stream().map(Moves::getName);}
}
