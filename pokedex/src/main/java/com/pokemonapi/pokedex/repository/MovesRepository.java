package com.pokemonapi.pokedex.repository;

import com.pokemonapi.pokedex.models.Moves;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovesRepository extends JpaRepository<Moves, Long> {

}