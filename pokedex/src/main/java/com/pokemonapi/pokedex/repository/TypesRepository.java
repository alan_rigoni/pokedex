package com.pokemonapi.pokedex.repository;

import com.pokemonapi.pokedex.models.Types;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TypesRepository extends JpaRepository<Types, Long> {
}