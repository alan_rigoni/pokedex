package com.pokemonapi.pokedex.repository;

import com.pokemonapi.pokedex.models.Abilities;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AbilitiesRepository extends JpaRepository<Abilities, Long> {

}