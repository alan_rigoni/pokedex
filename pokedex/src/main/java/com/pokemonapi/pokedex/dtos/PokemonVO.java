package com.pokemonapi.pokedex.dtos;

import com.pokemonapi.pokedex.models.Pokemon;
import com.pokemonapi.pokedex.service.AbilitiesService;
import com.pokemonapi.pokedex.service.TypesService;
import com.pokemonapi.pokedex.service.MovesService;
import com.pokemonapi.pokedex.service.PokemonServices;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

//Aqui é o value object do Pokemon (é oq será apresentado ao usuário)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class PokemonVO {
    //Variáveis
    private String id;
    private String name;
    @JsonProperty("base_experience")
    private int baseExperience;
    private int weight;
    //Variáveis com Arrays inclusos
    private List<MovesVO> moves = new ArrayList<>();
    private List<AbilitiesVO> abilities = new ArrayList<>();
    private List<TypesVO> types = new ArrayList<>();

    //Pega o Id
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    //Pega o nome e seta o Nome
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    //Pega o Experiência e seta a Experiência
    public int getBaseExperience() {
        return baseExperience;
    }
    public void setBaseExperience(int baseExperience) {
        this.baseExperience = baseExperience;
    }

    //Pega o Peso e seta o Peso
    public int getWeight() {
        return weight;
    }
    public void setWeight(int weight) {
        this.weight = weight;
    }

    /*Ordena em lista as habilidades da classe AbilitiesVO cria o método getAbilities e retorna o valor
    para a variável abilities deste arquivo, assim gerando um novo Array com as habilidades.
    O AbilitiesVO serve como ponte para acessar os detalhes das habilidades*/
    public List<AbilitiesVO> getAbilities() {return abilities;}
    public void setAbilities(List<AbilitiesVO> abilities) {this.abilities = abilities;}

    /*Ordena em lista os movimentos da classe MovesVO cria o método getMoves e retorna o valor
    para a variável moves deste arquivo, assim gerando um novo Array com os movimentos.
    O MovesVO serve como ponte para acessar os detalhes de movimentos*/
    public List<MovesVO> getMoves() {return moves;}
    public void setMoves(List<MovesVO> moves) {this.moves = moves;}

    /*Ordena em lista os tipos da classe TypesVO cria o método getTypes e retorna o valor
    para a variável types deste arquivo, assim gerando um novo Array com os tipos.
    O TypesVO serve como ponte para acessar os detalhes dos tipos*/
    public List<TypesVO> getTypes() {return types;}
    public void setTypes(List<TypesVO> types) {this.types = types;}

    //Conversão para a classe Pokemon onde utiliza os serviços para obtenção de dados
    public Pokemon converter(PokemonServices pokemonService, AbilitiesService abilitiesService,
                             MovesService movesService, TypesService typesService) {

        //Cria um novo objeto Pokemon e designa a informação para o mesmo
        Pokemon pokemon = new Pokemon();
        pokemon.setIdPokemon(this.getId());
        pokemon.setName(this.getName());
        pokemon.setBaseExperience(this.getBaseExperience());
        pokemon.setWeight(this.getWeight());

        /*Cria uma variável para a classe Pokemon onde pokemonSave vai entrar em contato com o
        pokemonService e dentro do service tem o método save*/
        final Pokemon pokemonSave = pokemonService.save(pokemon);

        //Ordena as habilidades e joga para a variável abilitiesModels
        List<com.pokemonapi.pokedex.models.Abilities> abilitiesModels = this.getAbilities().stream()
                //Aqui vai mapear dentro do array de abilidades e o ability é o mesmo nome da PokeApi
                .map(ability -> {
                    //Determina a variável abilitiesDetail para acessar AbilitiesDetailsVO
                    AbilitiesDetailsVO abilitiesDetail = ability.getAbility();
                    com.pokemonapi.pokedex.models.Abilities abilities = new com.pokemonapi.pokedex.models.Abilities();
                    abilities.setName(abilitiesDetail.getName());
                    abilities.setUrl(abilitiesDetail.getUrl());
                    abilities.setPokemon(pokemonSave);
                    return abilitiesService.save(abilities);
                }).collect(Collectors.toList());
        pokemonSave.setAbilities(abilitiesModels);

        //Ordena os movimentos e joga para a variável movesModels
        List<com.pokemonapi.pokedex.models.Moves> movesModels = this.getMoves().stream()
                //Aqui vai mapear dentro do array de movimentos e o move é o mesmo nome da PokeApi
                .map(move -> {
                    //Determina a variável movesDetail para acessar MovesDetailsVO
                    MovesDetailsVO movesDetail = move.getMove();
                    com.pokemonapi.pokedex.models.Moves moves = new com.pokemonapi.pokedex.models.Moves();
                    moves.setName(movesDetail.getName());
                    moves.setUrl(movesDetail.getUrl());
                    moves.setPokemon(pokemonSave);
                    return movesService.save(moves);
                }).collect(Collectors.toList());
        pokemonSave.setMoves(movesModels);

        //Ordena as tipos e joga para a variável typesModels
        List<com.pokemonapi.pokedex.models.Types> typesModels = this.getTypes().stream()
                //Aqui vai mapear dentro do array de tipos e o type é o mesmo nome da PokeApi
                .map(type -> {
                    //Determina a variável typesDetail para acessar TypesDetailsVO
                    TypesDetailsVO typesDetail = type.getType();
                    com.pokemonapi.pokedex.models.Types types = new com.pokemonapi.pokedex.models.Types();
                    types.setName(typesDetail.getName());
                    types.setUrl(typesDetail.getUrl());
                    types.setPokemon(pokemonSave);
                    return typesService.save(types);
                }).collect(Collectors.toList());
        pokemonSave.setTypes(typesModels);

        return pokemonSave;
    }
}