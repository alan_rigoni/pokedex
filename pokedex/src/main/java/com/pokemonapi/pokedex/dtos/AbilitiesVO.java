package com.pokemonapi.pokedex.dtos;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

//Classe criada para acessar os detalhes
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class AbilitiesVO {
    //Declarar o mesmo nome da variável da PokeApi para pegar os detalhes
    private AbilitiesDetailsVO ability;

    //Getters e Setters definido para uso da variável
    public AbilitiesDetailsVO getAbility() {return ability;}
    public void setAbility(AbilitiesDetailsVO ability) {this.ability = ability;}
}
