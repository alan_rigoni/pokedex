package com.pokemonapi.pokedex.dtos;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

//Classe criada para acessar os detalhes
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class MovesVO {
    //Declarar o mesmo nome da variável da PokeApi para pegar os detalhes
    private MovesDetailsVO move;

    //Getters e Setters definido para uso da variável
    public MovesDetailsVO getMove() {return move;}
    public void setMove(MovesDetailsVO move) {this.move = move;}

}
