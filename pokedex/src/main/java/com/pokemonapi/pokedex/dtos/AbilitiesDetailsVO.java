package com.pokemonapi.pokedex.dtos;
import com.fasterxml.jackson.annotation.JsonAutoDetect;

//Classe contendo os detalhes das habilidades
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class AbilitiesDetailsVO {
    //Declaração de variáveis
    private String name;
    private String url;

    //Getters e Setters para uso das variáveis
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
}
