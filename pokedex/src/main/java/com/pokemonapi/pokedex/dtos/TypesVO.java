package com.pokemonapi.pokedex.dtos;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

//Classe criada para acessar os detalhes
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class TypesVO {
    //Declarar o mesmo nome da variável da PokeApi para pegar os detalhes
    private TypesDetailsVO type;

    //Getters e Setters definido para uso da variável
    public TypesDetailsVO getType() {return type;}
    public void setType(TypesDetailsVO type) {this.type = type;}
}
