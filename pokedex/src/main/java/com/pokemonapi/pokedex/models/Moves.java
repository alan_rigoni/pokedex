package com.pokemonapi.pokedex.models;

import javax.persistence.*;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Entity
@Data
public class Moves {
  //Cria váriavel PK da tabela do Banco
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  //Variáveis
  private String name;
  private String url;

  //Define o Id fornecido pela PokeApi com FK
  @ManyToOne
  @JoinColumn(name = "pokemon_id", foreignKey = @ForeignKey(name = "fk_move_pokemon"))
  private Pokemon pokemon;

  //Pega o Id e seta o Id que será gerado pelo JPA
  public long getId() {return id;}
  public void setId(long id) {this.id = id;}

  //Pega o nome do movimento e seta de acordo com o nome do movimento da PokeAPI
  public String getName() {return name;}
  public void setName(String name) {this.name = name;}

  //Pega a URL do movimento e seta a URL de acordo com a URL do movimento da PokeAPI
  public String getUrl() {return url;}
  public void setUrl(String url) {this.url = url;}

  //Pega o Pokemon da classe Pokemon e seta os valores de Move para o Pokemon (na classe Pokemon)
  public Pokemon getPokemon() {return pokemon;}
  public void setPokemon(Pokemon pokemon) {this.pokemon = pokemon;}
}
