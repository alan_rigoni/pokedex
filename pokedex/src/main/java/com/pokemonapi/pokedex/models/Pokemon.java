package com.pokemonapi.pokedex.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Getter
@Setter
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Pokemon implements Serializable {

    //Cria váriavel PK da tabela do Banco
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private long Id;

    //Define o Id fornecido pela PokeApi como valor único
    @Column(unique = true,nullable = false)
    private String idPokemon;

    //Variáveis
    @Column(unique = true, nullable = false)
    private String name;

    @Column(unique = true, nullable = false)
    private int baseExperience;

    @Column(unique = true, nullable = false)
    private int weight;

    @OneToMany(mappedBy = "pokemon", cascade = CascadeType.ALL,
    orphanRemoval = true, fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Abilities> abilities;

    @OneToMany(mappedBy = "pokemon", cascade = CascadeType.ALL,
            orphanRemoval = true, fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Moves> moves;

    @OneToMany(mappedBy = "pokemon", cascade = CascadeType.ALL,
            orphanRemoval = true, fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Types> types;

}
